//#include <AsgTools/MessageCheck.h>
//Local includes
#include "NTupAnalyser/Common.h"
#include "NTupAnalyser/TruthHelper.h"
#include "NTupAnalyser/HistosSave.h"
#include "NTupAnalyser/HcAnalyse.h"

//#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/EgammaContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/EgammaEnums.h>
#include <xAODPrimitives/IsolationType.h>
#include <xAODTracking/TrackingPrimitives.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"

// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODCore/ShallowAuxContainer.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"

//Root includes
#include <TFile.h>
#include <TSystem.h>
#include <TTree.h>
#include <vector>
#include <string>



// this is needed to distribute the algorithm to the workers
ClassImp(HcAnalyse)


#define EL_RETURN_CHECK( CONTEXT, EXP )			\
do {							\
  if( ! EXP.isSuccess() ) {				\
    Error( CONTEXT,					\
    XAOD_MESSAGE( "Failed to execute: %s" ),	\
    #EXP );					\
    return EL::StatusCode::FAILURE;			\
    }							\
    } while( false )



//Constructor
HcAnalyse :: HcAnalyse (void)
: m_hStore(nullptr)
{ }

EL::StatusCode HcAnalyse :: setupJob (EL::Job& job)
{
  job.useXAOD ();
  EL_RETURN_CHECK( "setupJOB()", xAOD::Init());
  return EL::StatusCode::SUCCESS;
}

//Initialising the histos for the Plots
EL::StatusCode HcAnalyse :: histInitialize ()
{
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "Initialising the histograms... " << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
	//Initializing the histogram store
	m_hStore = new HistosSave();

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode HcAnalyse :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HcAnalyse :: changeInput (bool /*firstFile*/)
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HcAnalyse :: initialize ()
{
	m_eventCounter = 0;
	xAOD::TEvent* event = wk()->xaodEvent();

	Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

	//Creating the histograms depending on MC (truth or reco) or Data
	createHistos();

	TFile *outputFile = wk()->getOutputFile (_outputName);
    tree = new TTree ("tree", "tree");
    tree->SetDirectory (outputFile);

    tree->Branch("int_weights",   	        &initial_weight);
    tree->Branch("tot_weights", 	        &total_weights);
    tree->Branch("analys_weight",           &weight_analyse);
    tree->Branch("mass_yy", 	            &mass_yy);

	return StatusCode::SUCCESS;
}


EL::StatusCode HcAnalyse :: execute ()
{

    xAOD::TEvent* event = wk()->xaodEvent();
	int statSize=1;
	if(m_eventCounter!=0){
		double power=std::floor(log10(m_eventCounter));
		statSize=(int)std::pow(10.,power);
	}
	if(m_eventCounter%statSize==0) std::cout << "Event: " << m_eventCounter << std::endl;
	m_eventCounter++;


    //--------------------------------------------------------------------------------------------------
    //           Event information
    //--------------------------------------------------------------------------------------------------
    //EventInfo//////////////////////////////////
    const xAOD::EventInfo *eventInfo = 0;
    if (! event->retrieve(eventInfo, "EventInfo").isSuccess()){
    	Error("execute()", "Failed to retrieve event info collection. Exiting.");
    	return EL::StatusCode::FAILURE;}
    isMC = false; if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){  isMC = true;}
    //HGamEventInfo/////////////////////////////
    const xAOD::EventInfo *HgameventInfo = 0;
    if (! event->retrieve(HgameventInfo, "HGamEventInfo").isSuccess()) {
    	Error("execute()", "Failed to retrieve Hgam event info collection. Exiting.");
    	return EL::StatusCode::FAILURE;}


	//--------------------------------------------------------------------------------------------------
	//----------------------------------------------
	//            Initialize the containers
	//----------------------------------------------
	const xAOD::PhotonContainer* photons = 0;
	const xAOD::JetContainer* jets = 0;
	const xAOD::ElectronContainer* electrons = 0;
	const xAOD::MuonContainer* muons = 0;
	const xAOD::TruthParticleContainer *truphotons = 0;

    //Jets//Photons//Electrons//Muons
    EL_RETURN_CHECK("execute()",event->retrieve( jets, _reco_jet_collection.c_str() ));//pass the string of your favorite jet container
    EL_RETURN_CHECK("execute()",event->retrieve( photons, "HGamPhotons" ));
    EL_RETURN_CHECK("execute()",event->retrieve( electrons, "HGamElectrons" ));
    EL_RETURN_CHECK("execute()",event->retrieve( muons, "HGamMuons" ));
    //--------------------------------------------------------------------------------------------------

	//--------------------------------------------------------------------------------------------------
	//MAke truth histograms
    if(isMC){
    	//TruthHGamEvent info///////////////////////
		const xAOD::EventInfo *HgamTrutheventInfo = 0;
		if (! event->retrieve(HgamTrutheventInfo, "HGamTruthEventInfo").isSuccess()){
		Error("execute()", "Failed to retrieve Hgam event info collection. Exiting.");
		return EL::StatusCode::FAILURE;}
    	EL_RETURN_CHECK("execute()",event->retrieve( truphotons, "HGamTruthPhotons" )); if(_istruth)dotruth(truphotons,HgamTrutheventInfo);
    }
    //--------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------
    //Do the cutflow of the NTuples  (pass the boolean - do_cutflow?)
    if(_do_Cutflow){
        //Checking the number of events
        std::cout << "-------------------------------------------------------" << std::endl;
		std::cout << "Checking the total number of events.... " << m_eventCounter << std::endl;
		std::cout << "-------------------------------------------------------"  << std::endl;
		fillCutFlow(CutEnum(ALL));
		std::cout << "-------------------------------------------------------" << std::endl;
		std::cout << "Selecting Higgs candidate.... "  << std::endl;
		std::cout << "-------------------------------------------------------"  << std::endl;
		//1. preselected photons
		if(HgameventInfo->auxdataConst<char>("isPassedPreselection")<1) {return EL::StatusCode::SUCCESS;}fillCutFlow(CutEnum(PRESEL));
		//2. passed trigger match (both)
		if(HgameventInfo->auxdataConst<char>("isPassedTriggerMatch")<1) {return EL::StatusCode::SUCCESS;}fillCutFlow(CutEnum(MATCH));
		//3. passed Tight PID
		if(HgameventInfo->auxdataConst<char>("isPassedPID")<1) {return EL::StatusCode::SUCCESS;}fillCutFlow(CutEnum(TIGHTID));
		//4. passed Isolation
		if(HgameventInfo->auxdataConst<char>("isPassedIsolation")<1) {return EL::StatusCode::SUCCESS;}fillCutFlow(CutEnum(ISO));
		//5. passed rel. pT cut
		//so the pt will be on the plato - the triggers reach the Plato at particular pT
		if(HgameventInfo->auxdataConst<char>("isPassedRelPtCuts")<1) {return EL::StatusCode::SUCCESS;}fillCutFlow(CutEnum(RELPT));
		//6. passed mass cut
		if(HgameventInfo->auxdataConst<char>("isPassedMassCut")<1) {return EL::StatusCode::SUCCESS;}fillCutFlow(CutEnum(MASS));

		std::cout << "------------------------------------------------------------" << std::endl;
		std::cout << "Now, we have a good Higgs candidate. Lets check the weights " << std::endl;
		std::cout << "------------------------------------------------------------" << std::endl;
    }//end of cutflow
    //--------------------------------------------------------------------------------------------------

	//--------------------------------------------------------------------------------------------------
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "Checking the weights......... if we are dealing with MC" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
    //Checking the weights which include the InitialWeights*SF
    WeightsScales(HgameventInfo);
    //--------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------
    auto leading_photon = (*photons)[0];
    auto subleading_photon = (*photons)[1];
    if(_ispTcut){ //we cut on the pt cut for the photon which is 25 GeV and 22
        if((leading_photon->pt() < _phpTcut * HC::GeV ) || (subleading_photon->pt() < _phpTcut * HC::GeV))return EL::StatusCode::SUCCESS;
    }
    //Cutflow
    fillCutFlow(CutEnum(PHPT));


    //Here is the mass before the jet cut
    mass_gev = HgameventInfo->auxdataConst<float>("m_yy") * HC::invGeV;
    //if(mass_gev>130.)return EL::StatusCode::SUCCESS;
    //if(mass_gev<120.)return EL::StatusCode::SUCCESS;

    HStore()->fillTH1F("m_yy_nojet",mass_gev,total_weights);

    //--------------------------------------------------------------------------------------------------
    std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << " We make the cuts on number of jets, Njets>0 and jet pT" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;

    //Cutting on number og jets in the events
    int n_jets = jets->size();
    if(n_jets < 1 )return EL::StatusCode::SUCCESS;
    fillCutFlow(CutEnum(JET1));
    //--------------------------------------------------------------------------------------------------

    //the masses calculated by hand and m_yy form Ntup are identical! -------------------------(checked)
    //Fill the histo (without and with jvt weight) for the at least 1 jet
    HStore()->fillTH1F("m_yy_1j_jvt",mass_gev,weight_analyse);
    //--------------------------------------------------------------------------------------------------


	//Taking only the leading jet
	auto tleadjet = (*jets)[0];
/*
	if(fabs(tleadjet->eta())>2.5) return EL::StatusCode::SUCCESS;
	fillCutFlow(CutEnum(LEADJET2p5));
	if(tleadjet->pt()  < 20 * HC::GeV) return EL::StatusCode::SUCCESS;
	fillCutFlow(CutEnum(LEADJET25));
	HStore()->fillTH1F("m_yy_leadj25_jvt",mass_gev,weight_analyse);
	HStore()->fillTH1F("weights_after_ptetacut",weight_analyse);
*/

	if(fabs(tleadjet->eta())<2.5 && tleadjet->pt() > 20 * HC::GeV){HStore()->fillTH1F("m_yy_leadj20_jvt",mass_gev,weight_analyse20);}
	if(fabs(tleadjet->eta())<2.5 && tleadjet->pt() > 25 * HC::GeV){HStore()->fillTH1F("m_yy_leadj25_jvt",mass_gev,weight_analyse);}
	if(fabs(tleadjet->eta())<2.5 && tleadjet->pt() > 30 * HC::GeV){HStore()->fillTH1F("m_yy_leadj30_jvt",mass_gev,weight_analyse30);}
	if(fabs(tleadjet->eta())<2.5 && tleadjet->pt() > 35 * HC::GeV){HStore()->fillTH1F("m_yy_leadj35_jvt",mass_gev,weight_analyse35);}

	//--------------------------------------------------------------------------------------------------
    std::cout << "Finished with weights ... starting to check jets and optimize them..." << std::endl;
    //Check the jsets with different pT and /or check the optimization
    checkJets(HgameventInfo,jets,photons,weight_analyse,weight_analyse20,weight_analyse30,weight_analyse35);
    //--------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------
    std::cout << "Jets are done. Checking the photons and making some histos..." << std::endl;
    //Checking he distributions of photons
    if(_isreco)doreco(photons,jets);
    //--------------------------------------------------------------------------------------------------


     tree->Fill();
  return StatusCode::SUCCESS;
}



EL::StatusCode HcAnalyse :: postExecute (){ return EL::StatusCode::SUCCESS;}


EL::StatusCode HcAnalyse :: finalize ()
{
  if(_do_Cutflow){
  	printf("\nEvent selection cut flow:\n");
  	hist_out=getCutFlowHisto();
  	printCutFlowHisto(hist_out,0);
  }

  SafeDelete(m_hStore);
  return StatusCode::SUCCESS;
}

EL::StatusCode HcAnalyse :: histFinalize ()
{
  std::cout<<"Events = "<<m_eventCounter<<std::endl;
  return EL::StatusCode::SUCCESS;
}

//Some functions used in the processing
//--------------------------------------------------------------------------------------------------
void HcAnalyse::printCutFlowHisto(TH1F *h, int Ndecimals)
{
  TString format("  %-24s%10.");
  format += Ndecimals;
  format += "f%11.2f%%%11.2f%%\n";
  int all_bin = h->FindBin(0);
  printf("  %-24s%10s%12s%12s\n", "Event selection", "Nevents", "Cut rej.", "Tot. eff.");

  for (int bin = 1; bin <= h->GetNbinsX(); ++bin) {
    double ntot = h->GetBinContent(all_bin), n = h->GetBinContent(bin), nprev = h->GetBinContent(bin - 1);
    TString cutName(h->GetXaxis()->GetBinLabel(bin));
    cutName.ReplaceAll("#it{m}_{#gamma#gamma}", "m_yy");

    if (bin == 1 || nprev == 0 || n == nprev)
    { printf(format.Data(), cutName.Data(), n, -1e-10, n / ntot * 100); }
    else // if the cut does something, print more information
    { printf(format.Data(), cutName.Data(), n, (n - nprev) / nprev * 100, n / ntot * 100); }
  }
}

//--------------------------------------------------------------------------------------------------
//Dealing with the weights:
void HcAnalyse::WeightsScales(const xAOD::EventInfo* HgameventInfo)
{

    //==1. - initial weight - to be used for truth histos (mcWeight * pileupWeight * vertexWeight)-----(checked)
    initial_weight = HgameventInfo->auxdataConst<float>("weightInitial");

    float final_weight  = HgameventInfo->auxdataConst<float>("weight");
    float jvt25 = HgameventInfo->auxdataConst<float>("weightJvt_25");//weight for the jets above 25 GeV
    float jvt20 = HgameventInfo->auxdataConst<float>("weightJvt_20");//weight for the jets above 25 GeV
    float jvt30 = HgameventInfo->auxdataConst<float>("weightJvt_30");//weight for the jets above 25 GeV
    float jvt35 = HgameventInfo->auxdataConst<float>("weightJvt_35");//weight for the jets above 25 GeV

    //weight_sf - already includes the weights from Reco*Trig for muons--------------------(checked)
    float weight_sf = HgameventInfo->auxdataConst<float>("weightSF");
    //==2. - total weight - to be used for the reco histos - the same as final weight
    total_weights = initial_weight * weight_sf; tot_weight_nosf = initial_weight * jvt25;
    //Fill the histos
    HStore()->fillTH1F("total_weights_calc",total_weights);
    HStore()->fillTH1F("total_weights_Ntup",final_weight);//the same -->checked

    //With the JVT weights
    //==3. - Calculate the total weight with jvt
    weight_analyse = total_weights * jvt25;
    weight_analyse20 = total_weights * jvt20;
    weight_analyse30 = total_weights * jvt30;
    weight_analyse35 = total_weights * jvt35;

    //Fill the histo
    HStore()->fillTH1F("jvt_plus_total",weight_analyse);
    HStore()->fillTH1F("jvt_weight",jvt25);



}
//--------------------------------------------------------------------------------------------------
//Dealing with jet and ctag optimization
EL::StatusCode HcAnalyse::checkJets(const xAOD::EventInfo* HgameventInfo,const xAOD::JetContainer *jets, const xAOD::PhotonContainer *photon, float weightjvt, float weightjvt20, float weightjvt30, float weightjvt35)
{
/*
		auto leadjet = (*jets)[0];
		//Lets make the photon cuts
		auto pho_lead = (*photon)[0];
	    auto pho_sublead = (*photon)[1];
		TLorentzVector pho1 = pho_lead->p4(), pho2 = pho_sublead->p4();
	    pho1 *= HC::invGeV; pho2 *= HC::invGeV; TLorentzVector Higgs = pho1+pho2;

	    TLorentzVector jet = leadjet->p4();
	    jet *= HC::invGeV;
	    TLorentzVector jgamgam = jet + Higgs;

	    dr05_nomhj = false, dr10_nomhj = false, dr15_nomhj = false;
	    dr05_mhj150 = false, dr05_mhj155 = false, dr05_mhj160 = false, dr05_mhj165 = false;
	    dr10_mhj150 = false, dr10_mhj155 = false, dr10_mhj160 = false, dr10_mhj165 = false;
	    dr15_mhj150 = false, dr15_mhj155 = false, dr15_mhj160 = false, dr15_mhj165 = false;

		if (jet.DeltaR(pho1)>0.5 && jet.DeltaR(pho2)>0.5){
	        	HStore()->fillTH1F("m_yy_dR05_mjhdef_jvt",mass_gev,weightjvt); dr05_nomhj = true;
	        	if(jgamgam.M()>150){ HStore()->fillTH1F("m_yy_dR05_mjh150_jvt",mass_gev,weightjvt); dr05_mhj150 = true;}
	        	if(jgamgam.M()>155){ HStore()->fillTH1F("m_yy_dR05_mjh155_jvt",mass_gev,weightjvt); dr05_mhj155 = true;}
	        	if(jgamgam.M()>160){ HStore()->fillTH1F("m_yy_dR05_mjh160_jvt",mass_gev,weightjvt); dr05_mhj160 = true;}
	        	if(jgamgam.M()>165){ HStore()->fillTH1F("m_yy_dR05_mjh165_jvt",mass_gev,weightjvt); dr05_mhj165 = true;}
	        }

	        if (jet.DeltaR(pho1)>1. && jet.DeltaR(pho2)>1.){
	        	HStore()->fillTH1F("m_yy_dR1_mjhdef_jvt",mass_gev,weightjvt); dr10_nomhj = true;
	        	if(jgamgam.M()>150){ HStore()->fillTH1F("m_yy_dR1_mjh150_jvt",mass_gev,weightjvt); dr10_mhj150 = true;}
	        	if(jgamgam.M()>155){ HStore()->fillTH1F("m_yy_dR1_mjh155_jvt",mass_gev,weightjvt); dr10_mhj155 = true;}
	        	if(jgamgam.M()>160){ HStore()->fillTH1F("m_yy_dR1_mjh160_jvt",mass_gev,weightjvt); dr10_mhj160 = true;}
	        	if(jgamgam.M()>165){ HStore()->fillTH1F("m_yy_dR1_mjh165_jvt",mass_gev,weightjvt); dr10_mhj165 = true;}
	        }

	        if (jet.DeltaR(pho1)>1.5 && jet.DeltaR(pho2)>1.5){
	        	HStore()->fillTH1F("m_yy_dR15_mjhdef_jvt",mass_gev,weightjvt); dr15_nomhj = true;
	        	if(jgamgam.M()>150){ HStore()->fillTH1F("m_yy_dR15_mjh150_jvt",mass_gev,weightjvt); dr15_mhj150 = true;}
	        	if(jgamgam.M()>155){ HStore()->fillTH1F("m_yy_dR15_mjh155_jvt",mass_gev,weightjvt); dr15_mhj155 = true;}
	        	if(jgamgam.M()>160){ HStore()->fillTH1F("m_yy_dR15_mjh160_jvt",mass_gev,weightjvt); dr15_mhj160 = true;}
	        	if(jgamgam.M()>165){ HStore()->fillTH1F("m_yy_dR15_mjh165_jvt",mass_gev,weightjvt); dr15_mhj165 = true;}
	        }


		//===================
		//extract the MV2 scores
		bool mv2_60 = false;
		if(leadjet->auxdataConst<char>("MV2c10_FixedCutBEff_60")==1) {mv2_60 = true;}
		if(mv2_60==false){ HStore()->fillTH1F("m_yy_j25_mv260_jvt",mass_gev,weightjvt); fillCutFlow(CutEnum(LEADJETNOBTAG));}


		//===================
		//Extract the DL1 scores
		double pu = leadjet->auxdataConst<double>("DL1_pu");
		double pc = leadjet->auxdataConst<double>("DL1_pc");
		double pb = leadjet->auxdataConst<double>("DL1_pb");
		//DL1 cuts
		double dl1_discriminant = discriminant(pc,pb,pu,0.01);
		int countcjets_b60 = 0;
		//if(mv2_60==false && dl1_discriminant > 0.4 ) {countcjets_b60++;}


        if(mv2_60==false && dl1_discriminant > 0.4 && dr05_nomhj==true){HStore()->fillTH1F("m_yy_dR05_mjhdefc_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr10_nomhj==true){HStore()->fillTH1F("m_yy_dR1_mjhdefc_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr15_nomhj==true){HStore()->fillTH1F("m_yy_dR15_mjhdefc_jvt",mass_gev,weightjvt);}

        if(mv2_60==false && dl1_discriminant > 0.4 && dr05_mhj150==true){HStore()->fillTH1F("m_yy_dR05_mjh150c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr05_mhj155==true){HStore()->fillTH1F("m_yy_dR05_mjh155c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr05_mhj160==true){HStore()->fillTH1F("m_yy_dR05_mjh160c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr05_mhj165==true){HStore()->fillTH1F("m_yy_dR05_mjh165c_jvt",mass_gev,weightjvt);}

        if(mv2_60==false && dl1_discriminant > 0.4 && dr10_mhj150==true){HStore()->fillTH1F("m_yy_dR1_mjh150c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr10_mhj155==true){HStore()->fillTH1F("m_yy_dR1_mjh155c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr10_mhj160==true){HStore()->fillTH1F("m_yy_dR1_mjh160c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr10_mhj165==true){HStore()->fillTH1F("m_yy_dR1_mjh165c_jvt",mass_gev,weightjvt);}

        if(mv2_60==false && dl1_discriminant > 0.4 && dr15_mhj150==true){HStore()->fillTH1F("m_yy_dR15_mjh150c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr15_mhj155==true){HStore()->fillTH1F("m_yy_dR15_mjh155c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr15_mhj160==true){HStore()->fillTH1F("m_yy_dR15_mjh160c_jvt",mass_gev,weightjvt);}
        if(mv2_60==false && dl1_discriminant > 0.4 && dr15_mhj165==true){HStore()->fillTH1F("m_yy_dR15_mjh165c_jvt",mass_gev,weightjvt);}


    	//if(countcjets_b60>0){
    	if(mv2_60==false && dl1_discriminant > 0.4 ){
    		HStore()->fillTH1F("m_yy_j25_mv260_ctag",mass_gev,weightjvt);
		    //HStore()->fillTH1F("total_weights_jvt",weightjvt);
		    HStore()->fillTH1F("jet_pT",leadjet->pt()*HC::invGeV);

    		fillCutFlow(CutEnum(LEADJETCTAG));

    	   //Lets make the photon cuts
		   auto pho_lead = (*photon)[0];
	       auto pho_sublead = (*photon)[1];
		   TLorentzVector pho1 = pho_lead->p4(), pho2 = pho_sublead->p4();
	       pho1 *= HC::invGeV; pho2 *= HC::invGeV; TLorentzVector Higgs = pho1+pho2;

	       TLorentzVector jet = leadjet->p4();
	       jet *= HC::invGeV;
	       TLorentzVector jgamgam = jet + Higgs;


	        HStore()->fillTH1F("dR_y1_j",jet.DeltaR(pho1),weightjvt);
	        HStore()->fillTH1F("dR_y2_j",jet.DeltaR(pho2),weightjvt);
	        HStore()->fillTH1F("p1pt",pho1.Pt(),weightjvt);
	        HStore()->fillTH1F("p2pt",pho2.Pt(),weightjvt);

	        HStore()->fillTH1F("jgamgam_dR_b",jet.DeltaR(Higgs),weightjvt);
	        HStore()->fillTH1F("jgamgam_dphi_b",jet.DeltaPhi(Higgs),weightjvt);
	        HStore()->fillTH1F("jgamgam_dy_b",jet.Rapidity()- Higgs.Rapidity(),weightjvt);
	        HStore()->fillTH1F("jgamgam_m_b",jgamgam.M(),weightjvt);
*/
/*
	        if (jet.DeltaR(pho1)>0.5 && jet.DeltaR(pho2)>0.5){
	        	HStore()->fillTH1F("m_yy_dR05_mjhdefc_jvt",mass_gev,weightjvt);
	        	if(jgamgam.M()>150){ HStore()->fillTH1F("m_yy_dR05_mjh150c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>155){ HStore()->fillTH1F("m_yy_dR05_mjh155c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>160){ HStore()->fillTH1F("m_yy_dR05_mjh160c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>165){ HStore()->fillTH1F("m_yy_dR05_mjh165c_jvt",mass_gev,weightjvt); }
	        }

	        if (jet.DeltaR(pho1)>1. && jet.DeltaR(pho2)>1.){
	        	HStore()->fillTH1F("m_yy_dR1_mjhdefc_jvt",mass_gev,weightjvt);
	        	if(jgamgam.M()>150){ HStore()->fillTH1F("m_yy_dR1_mjh150c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>155){ HStore()->fillTH1F("m_yy_dR1_mjh155c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>160){ HStore()->fillTH1F("m_yy_dR1_mjh160c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>165){ HStore()->fillTH1F("m_yy_dR1_mjh165c_jvt",mass_gev,weightjvt); }
	        }

	        if (jet.DeltaR(pho1)>1.5 && jet.DeltaR(pho2)>1.5){
	        	HStore()->fillTH1F("m_yy_dR15_mjhdefc_jvt",mass_gev,weightjvt);
	        	if(jgamgam.M()>150){ HStore()->fillTH1F("m_yy_dR15_mjh150c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>155){ HStore()->fillTH1F("m_yy_dR15_mjh155c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>160){ HStore()->fillTH1F("m_yy_dR15_mjh160c_jvt",mass_gev,weightjvt); }
	        	if(jgamgam.M()>165){ HStore()->fillTH1F("m_yy_dR15_mjh165c_jvt",mass_gev,weightjvt); }
	        }
*/

/*
    		if ( jet.DeltaR(pho1)>0.5 && jet.DeltaR(pho2)>0.5 ){
	        	HStore()->fillTH1F("m_yy_dR_jvt",mass_gev,weightjvt);
	        	fillCutFlow(CutEnum(DRYJ));
	        	if(pho1.Pt()>40 && pho2.Pt()>28){
	        		fillCutFlow(CutEnum(HighPt));
	        		HStore()->fillTH1F("m_yy_HighPt_jvt",mass_gev,weightjvt);
	        		HStore()->fillTH1F("leadCjet_pt",jet.Pt(),weightjvt);
	        		HStore()->fillTH1F("leadCjet_eta",jet.PseudoRapidity(),weightjvt);
	        		HStore()->fillTH1F("leadCjet_y",jet.Rapidity(),weightjvt);
	        		HStore()->fillTH1F("leadCjet_phi",jet.Phi(),weightjvt);
					HStore()->fillTH1F("leadCjet_m",jet.M(),weightjvt);
					if(jgamgam.M()>145){HStore()->fillTH1F("m_yy_JgamgamM_jvt",mass_gev,weightjvt);}
					HStore()->fillTH1F("jgamgam_m",jgamgam.M(),weightjvt);
					HStore()->fillTH1F("jgamgam_pt",jgamgam.Pt(),weightjvt);
					HStore()->fillTH1F("jgamgam_eta",jgamgam.PseudoRapidity(),weightjvt);
					HStore()->fillTH1F("jgamgam_y",jgamgam.Rapidity(),weightjvt);
					HStore()->fillTH1F("jgamgam_phi",jgamgam.Phi(),weightjvt);
					HStore()->fillTH1F("jgamgam_dR",jet.DeltaR(Higgs),weightjvt);
					HStore()->fillTH1F("jgamgam_dy",jet.Rapidity()-Higgs.Rapidity(),weightjvt);
					HStore()->fillTH1F("jgamgam_dphi",jet.DeltaPhi(Higgs),weightjvt);

	          	}//two hi pt photons

    	    }//dR cut

    	}//ctagging


*/




	//--------------------------------------------------------//
	//----------Performing the C tagging optimization---------//
	//--------------------------------------------------------//
    //Piece of code responsible for the sourting the events according the c tagging
    //this is needed for optimising the c tagging efficiency
    //here we do the fractions from 0 to 1 in steps of 0.05
    //DL cut from -5. to 5. in steps of 0.5
    //--------------------------------------------------------//

    if(_Dooptimization){
		for(int i = 0; i < bins; i++){//dl cut loop loop
			float score = count[i];
			for(int k = 0; k < fbins; k++){//fraction loop
				float bfrac = fcount[k];
				//define the variables -----------------------------------------//
				int countcjets_b60 = 0, countcjets_b70 = 0, countcjets_b77 = 0;
				int countTrueBmatch=0, countTrueCmatch=0, countTrueLmatch=0;
				int bmistag =0, ctag=0, lmistag =0;
				//--------------------------------------------------------------//
    			for(auto recoj: *jets){
    			    if(fabs(recoj->eta()) > 2.5)continue;//this is after version mistag_v3,v4
    				if(recoj->pt() < 20 * HC::GeV)continue;//this is after version mistag_v3,v4
    				bool isCjet = false, isBjet = false, isLjet = false;
    				//Checking the truth labeling
    				if(recoj->auxdataConst<int>("HadronConeExclTruthLabelID") == 5){isBjet = true;}
    				if(recoj->auxdataConst<int>("HadronConeExclTruthLabelID") == 4){isCjet = true;}
    				if(recoj->auxdataConst<int>("HadronConeExclTruthLabelID") == 0){isLjet = true;}
    				//setting the booleans
		    		bool truth_b = false; bool truth_l = false; bool truth_c = false;

					if(isBjet) {truth_b = true; countTrueBmatch++;}
					if(isCjet) {truth_c = true; countTrueCmatch++;}
					if(isLjet) {truth_l = true; countTrueLmatch++;}

		    		//DL1
					double pu_j = recoj->auxdataConst<double>("DL1_pu");
					double pc_j = recoj->auxdataConst<double>("DL1_pc");
					double pb_j = recoj->auxdataConst<double>("DL1_pb");
					//Dl
					double dl1_discriminant = discriminant(pc_j,pb_j,pu_j,bfrac);
					if(truth_b && (dl1_discriminant > score)) {bmistag++; }
					if(truth_c && (dl1_discriminant > score)) {ctag++; }
					if(truth_l && (dl1_discriminant > score)) {lmistag++; }

				}//end of reco jet loop


				N_recob[i][k]->Fill(countTrueBmatch);
				N_recoc[i][k]->Fill(countTrueCmatch);
				N_recol[i][k]->Fill(countTrueLmatch);

				N_tagb[i][k]->Fill(bmistag);
				N_tagc[i][k]->Fill(ctag);
				N_tagl[i][k]->Fill(lmistag);

				//2d map of the jets that are truth matched
				//if(countTrueCmatch > 0) {j_ctrue->SetBinContent(i,k,countTrueCmatch);}
				//j_btrue->SetBinContent(i,k,countTrueBmatch);
				//j_ltrue->SetBinContent(i,k,countTrueLmatch);

				//2d map of the jets that were truth matched but positivly identified as c
				//c_ctrue->SetBinContent(i,k,ctag);
				//c_btrue->SetBinContent(i,k,bmistag);
				//c_ltrue->SetBinContent(i,k,lmistag);


				float epsilon_b = 0.0, epsilon_c = 0.0, epsilon_l = 0.0;

				//misidentification B
				if(countTrueBmatch==0) {epsilon_b = 0.0;}
				else { epsilon_b = (bmistag*1.0)/(1.0*countTrueBmatch);}

				//identification C
				if(countTrueCmatch ==0) {epsilon_c = 0.0;}

				else { epsilon_c = (ctag*1.0)/(1.0*countTrueCmatch); }

				//misidentification L
				if(countTrueLmatch==0) {epsilon_l = 0.0;}
				else { epsilon_l = (lmistag*1.0)/(1.0*countTrueLmatch); }

				//filling the efficiency histograms
				if(countTrueBmatch > 0) {eff_bmiss[i][k]->Fill(epsilon_b); eff_bmissw[i][k]->Fill(epsilon_b,weightjvt20); eff_bmiss_nosf[i][k]->Fill(epsilon_b,tot_weight_nosf); eff_bmiss_init[i][k]->Fill(epsilon_b,initial_weight);}
				if(countTrueCmatch > 0) {eff_cmiss[i][k]->Fill(epsilon_c); eff_cmissw[i][k]->Fill(epsilon_c,weightjvt20); eff_cmiss_nosf[i][k]->Fill(epsilon_c,tot_weight_nosf); eff_cmiss_init[i][k]->Fill(epsilon_c,initial_weight);}
				if(countTrueLmatch > 0) {eff_lmiss[i][k]->Fill(epsilon_l); eff_lmissw[i][k]->Fill(epsilon_l,weightjvt20); eff_lmiss_nosf[i][k]->Fill(epsilon_l,tot_weight_nosf); eff_lmiss_init[i][k]->Fill(epsilon_l,initial_weight);}

				//Taking only the leading jet
				auto leadjet = (*jets)[0];
				if(fabs(leadjet->eta())>2.5)continue;
				fillCutFlow(CutEnum(LEADJET2p5));
				if(leadjet->pt() < 20 *HC::GeV)continue;
				fillCutFlow(CutEnum(LEADJET25));

				//Extract the DL1 scores
				double pu = leadjet->auxdataConst<double>("DL1_pu");
				double pc = leadjet->auxdataConst<double>("DL1_pc");
				double pb = leadjet->auxdataConst<double>("DL1_pb");

				//extract the MV2 scores
				bool mv2_60 = false, mv2_70 = false, mv2_77 = false;

				if(leadjet->auxdataConst<char>("MV2c10_FixedCutBEff_60")==1) {mv2_60 = true;}
				if(leadjet->auxdataConst<char>("MV2c10_FixedCutBEff_70")==1) {mv2_70 = true;}
				//if(leadjet->auxdataConst<char>("MV2c10_FixedCutBEff_77")==1) {mv2_77 = true;}

				if(mv2_60==false){masses_mv60_nodl1[i][k]->Fill(mass_gev,weightjvt20);}

				//DL1 cuts
				double dl1_discriminant = discriminant(pc,pb,pu,bfrac);
				//cout<<"The discriminant is "<< dl1_discriminant<<endl;
				//===================
				if(mv2_60==false and dl1_discriminant > score ) {countcjets_b60 ++; }
				if(mv2_70==false and dl1_discriminant > score ) {countcjets_b70 ++; }
				//if(mv2_77==false and dl1_discriminant > score ) {countcjets_b77 ++; }


				//lets make Ndl1cut x Nfrac histograms and store them in the root file  with names masses_Ndl1_Nfrac
				//float weights = weighttot * (1-0.25) * (1-0.05);
				//cout<<"My weight before "<<weightjvt<<endl;
				float weights = weightjvt20 * (1-epsilon_b) * (1-epsilon_l);

    	        if(countcjets_b60>0){masses_mv60[i][k]->Fill(mass_gev,weightjvt20);}
    	        if(countcjets_b70>0){masses_mv70[i][k]->Fill(mass_gev,weightjvt20);}
    	        //if(countcjets_b77>0){masses_mv77[i][k]->Fill(mass_gev,weightjvt);}

    	        //With frac weights
    	        if(countcjets_b60>0){masses_mv60w[i][k]->Fill(mass_gev,weights);}
    	        if(countcjets_b70>0){masses_mv70w[i][k]->Fill(mass_gev,weights);}
    	        //if(countcjets_b77>0){masses_mv77w[i][k]->Fill(mass_gev,weights);}


    	        if(i==0 and (k%10==0)){
    	            dl1_60[k]->Fill(dl1_discriminant,weightjvt20);
					dl1_70[k]->Fill(dl1_discriminant,weightjvt20);
    	            dl1_77[k]->Fill(dl1_discriminant,weightjvt20);

    	            //weight with fraction
    	            dl1_60w[k]->Fill(dl1_discriminant,weights);
    	            dl1_70w[k]->Fill(dl1_discriminant,weights);
    	            dl1_77w[k]->Fill(dl1_discriminant,weights);
    	        }


			}//end of fraction loop

		}//end of dl1 scores

	}//end of if statement do optimization



return EL::StatusCode::SUCCESS;

}

//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////
//Cutflow hist maker Inherited from the HGamTool////////////////
////////////////////////////////////////////
TH1F *HcAnalyse::makeCutFlowHisto(int id, TString suffix)
{
   // const char ids = "something";
   // TString name = Form("CutFlow_%s",ids);
    int Ncuts = s_cutDescs.size();
    TH1F *h = new TH1F(suffix, suffix, Ncuts, 0, Ncuts);
    for (int bin = 1; bin <= Ncuts; ++bin)
    { h->GetXaxis()->SetBinLabel(bin, s_cutDescs[bin - 1]); }
    wk()->addOutput(h);
	return h;
}

void HcAnalyse::fillCutFlow(CutEnum cut)
{
    getCutFlowHisto()->Fill(cut);
}


//--------------------------------------------------------------------------------------------------
//Discriminant function//very easy
double HcAnalyse::discriminant(double pc, double pb, double pu, float fraction)
{
    double ctag_discriminant = log( pc / ( (pb * fraction) + (1 - fraction)*pu ) );
    return ctag_discriminant;
}


//--------------------------------------------------------------------------------------------------
//creating the histos
EL::StatusCode HcAnalyse::createHistos()
{
	if(_isreco){
		//For the plots of reconstructed photons
		HStore()->createTH1F("myy", 60, 100., 160.,";#it{m}_{#gamma#gamma} [GeV]");
		HStore()->createTH1F("ptyy",200, 0., 200.,";#it{p}^{#gamma#gamma}_{T} [GeV]");
		HStore()->createTH1F("yyy", 25, -2.5, 2.5,";#it{y}^{#gamma#gamma}");
		HStore()->createTH1F("dryy", 100, 0., 10.,";#it{dr}^{#gamma#gamma}");
		HStore()->createTH1F("dyyy", 50, -5., 5.,";#it{dy}^{#gamma#gamma}");
		HStore()->createTH1F("dphiyy", 64, -8., 8.,";#it{dphi}^{#gamma#gamma}");
		//singles
		HStore()->createTH1F("y1pt",200, 0., 200.,";#it{p}^{#gamma}_{T} [GeV]");
		HStore()->createTH1F("y2pt",200, 0., 200.,";#it{p}^{#gamma}_{T} [GeV]");
		HStore()->createTH1F("y1eta",50,-2.5, 2.5,";#it{#eta}^{#gamma}");
		HStore()->createTH1F("y2eta",50,-2.5, 2.5,";#it{#eta}^{#gamma}");
		HStore()->createTH1F("y1phi",30,-3.2, 3.2,";#it{#phi}^{#gamma}");
		HStore()->createTH1F("y2phi",50,-3.2, 3.2,";#it{#phi}^{#gamma}");

		HStore()->createTH1F("dRjgam1", 100, 0., 10.,";#it{dr}^{#gamma{1}j}");
		HStore()->createTH1F("dRjgam2", 100, 0., 10.,";#it{dr}^{#gammф{2}j}");
		HStore()->createTH1F("dphijgam1", 64, -8., 8.,";#it{dphi}^{#gamma_{1}j}");
		HStore()->createTH1F("dphijgam2", 64, -8., 8.,";#it{dphi}^{#gamma_{2}j}");

		HStore()->createTH1F("jpt",400,0.,400.,";#it{p^{j}_{T}");
	    HStore()->createTH1F("jy",25, -2.5, 2.5,";#it{y}^{j}");
	    HStore()->createTH1F("jeta",25, -2.5, 2.5,";#it{#eta}^{j}");

		HStore()->createTH1F("jgamgam1_pt",400,0.,400.,";#it{p^{j#gamma#gamma}_{T}");
		HStore()->createTH1F("jgamgam1_m",400,0.,400.,";#it{M^{j#gamma#gamma}");
		HStore()->createTH1F("jgamgam1_phi",50,-3.2, 3.2,";#it{#phi^{j#gamma#gamma}");
		HStore()->createTH1F("jgamgam1_eta",25, -2.5, 2.5,";#it{#eta^{j#gamma#gamma}");
		HStore()->createTH1F("jgamgam1_y",25, -2.5, 2.5,";#it{y^{j#gamma#gamma}");

		HStore()->createTH1F("jh_dR", 100, 0., 10.,";#it{dr}^{#gamma{1}j}");
		HStore()->createTH1F("jh_dphi",64, -8., 8.,";#it{dr}^{#gamma{1}j}");
		HStore()->createTH1F("jh_dy", 50, -5., 5.,";#it{dr}^{#gamma{1}j}");




    }
    if(_istruth){	///Initializing the hisots
		//truth photons
		HStore()->createTH1F("htrue_m",60, 100, 160,";#it{m}_{#gamma#gamma}^{truth} [GeV]");
		HStore()->createTH1F("htrue_pt",100, 0., 200.,";#it{p}^{#gamma#gamma,truth}_{T} [GeV]");
		HStore()->createTH1F("htrue_y",25, -2.5, 2.5,";#it{y}^{#gamma#gamma,truth}");
		HStore()->createTH1F("htrue_dr",100, 0., 10.,";#it{dr}^{#gamma#gamma,truth}");
		HStore()->createTH1F("htrue_dy",50, -5., 5.,";#it{dy}^{#gamma#gamma,truth}");
		HStore()->createTH1F("htrue_phi",64, -8., 8.,";#it{dphi}^{#gamma#gamma,truth}");
		//singles
		HStore()->createTH1F("p1true_pt",100, 0., 200.,";#it{p}^{#gamma,truth}_{T} [GeV]");
		HStore()->createTH1F("p2true_pt",100, 0., 200.,";#it{p}^{#gamma,truth}_{T} [GeV]");
		HStore()->createTH1F("p1true_eta",50,-2.5, 2.5,";#it{#eta}^{#gamma,truth}");
		HStore()->createTH1F("p2true_eta",50,-2.5, 2.5,";#it{#eta}^{#gamma,truth}");
		HStore()->createTH1F("p1true_phi",30,-3.2, 3.2,";#it{#phi}^{#gamma,truth}");
		HStore()->createTH1F("p2true_phi",30,-3.2, 3.2,";#it{#phi}^{#gamma,truth}");

		//fiducial
		HStore()->createTH1F("htruefid_m",60, 100, 160,";#it{m}_{#gamma#gamma}^{truth,fid} [GeV]");
		HStore()->createTH1F("htruefid_pt",100, 0., 200.,";#it{p}^{#gamma#gamma,truth,fid}_{T} [GeV]");
		HStore()->createTH1F("htruefid_y",25, -2.5, 2.5,";#it{y}^{#gamma#gamma,truth,fid}");
		HStore()->createTH1F("htruefid_dr",100, 0., 10.,";#it{dr}^{#gamma#gamma,truth,fid}");
		HStore()->createTH1F("htruefid_dy",50, -5., 5.,";#it{dy}^{#gamma#gamma,truth,fid}");
		HStore()->createTH1F("htruefid_phi",64, -8., 8.,";#it{dphi}^{#gamma#gamma,truth,fid}");
		//fiducial singles
		HStore()->createTH1F("p1truefid_pt",100, 0., 200.,";#it{p}^{#gamma,truth,fid}_{T} [GeV]");
		HStore()->createTH1F("p2truefid_pt",100, 0., 200.,";#it{p}^{#gamma,truth,fid}_{T} [GeV]");
		HStore()->createTH1F("p1truefid_eta",50,-2.5, 2.5,";#it{#eta}^{#gamma,truth,fid}");
		HStore()->createTH1F("p2truefid_eta",50,-2.5, 2.5,";#it{#eta}^{#gamma,truth,fid}");
		HStore()->createTH1F("p1truefid_phi",30,-3.2, 3.2,";#it{#phi}^{#gamma,truth,fid}");
		HStore()->createTH1F("p2truefid_phi",30,-3.2, 3.2,";#it{#phi}^{#gamma,truth,fid}");


		//fiducial with c jet
		HStore()->createTH1F("chtruefid_m",60, 100, 160,";#it{m}_{#gamma#gamma}^{truth,fid} [GeV]");
		HStore()->createTH1F("chtruefid_pt",100, 0., 200.,";#it{p}^{#gamma#gamma,truth,fid}_{T} [GeV]");
		HStore()->createTH1F("chtruefid_y",25, -2.5, 2.5,";#it{y}^{#gamma#gamma,truth,fid}");
		HStore()->createTH1F("chtruefid_dr",100, 0., 10.,";#it{dr}^{#gamma#gamma,truth,fid}");
		HStore()->createTH1F("chtruefid_dy",50, -5., 5.,";#it{dy}^{#gamma#gamma,truth,fid}");
		HStore()->createTH1F("chtruefid_phi",64, -8., 8.,";#it{dphi}^{#gamma#gamma,truth,fid}");
		//fiducial singles
		HStore()->createTH1F("cp1truefid_pt",100, 0., 200.,";#it{p}^{#gamma,truth,fid}_{T} [GeV]");
		HStore()->createTH1F("cp2truefid_pt",100, 0., 200.,";#it{p}^{#gamma,truth,fid}_{T} [GeV]");
		HStore()->createTH1F("cp1truefid_eta",50,-2.5, 2.5,";#it{#eta}^{#gamma,truth,fid}");
		HStore()->createTH1F("cp2truefid_eta",50,-2.5, 2.5,";#it{#eta}^{#gamma,truth,fid}");
		HStore()->createTH1F("cp1truefid_phi",30,-3.2, 3.2,";#it{#phi}^{#gamma,truth,fid}");
		HStore()->createTH1F("cp2truefid_phi",30,-3.2, 3.2,";#it{#phi}^{#gamma,truth,fid}");
	}




	//general - optimizations
	HStore()->createTH1F("m_yy_j25", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_nojet", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	//jvt weight applied
	HStore()->createTH1F("m_yy_1j_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_j20_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	//after selecting leading jet
	HStore()->createTH1F("m_yy_leadj25_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_leadj20_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_leadj30_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_leadj35_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("weights_after_ptetacut", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("total_weights_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("m_yy_dR05_mjhdefc_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjhdefc_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjhdefc_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("m_yy_dR05_mjhdef_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjhdef_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjhdef_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

    //no c jets
	HStore()->createTH1F("m_yy_dR05_mjh150_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR05_mjh155_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR05_mjh160_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR05_mjh165_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("m_yy_dR1_mjh150_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjh155_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjh160_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjh165_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("m_yy_dR15_mjh150_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjh155_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjh160_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjh165_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	//with c jets
	HStore()->createTH1F("m_yy_dR05_mjh150c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR05_mjh155c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR05_mjh160c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR05_mjh165c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("m_yy_dR1_mjh150c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjh155c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjh160c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR1_mjh165c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("m_yy_dR15_mjh150c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjh155c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjh160c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("m_yy_dR15_mjh165c_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");





	HStore()->createTH1F("dR_y1_j", 100, 0., 10.,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("dR_y2_j", 100, 0., 10.,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("p1pt", 100, 0., 200.,";#it{m}_{#gamma#gamma} [GeV]");
	HStore()->createTH1F("p2pt", 100, 0., 200.,";#it{m}_{#gamma#gamma} [GeV]");

    HStore()->createTH1F("lead_jet_pt", 100, 0, 200,";Lead jet p_{T}[GeV]");

    HStore()->createTH1F("m_yy_HighPt_jvt",60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
    HStore()->createTH1F("m_yy_JgamgamM_jvt",60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("leadCjet_pt",300, 0, 300,";LeadC jet p_{T}[GeV]");
	HStore()->createTH1F("leadCjet_eta",25, -2.5, 2.5,";#it{#eta}^{j}");
	HStore()->createTH1F("leadCjet_y",25, -2.5, 2.5,";#it{y}^{j}");
	HStore()->createTH1F("leadCjet_phi",30,-3.2, 3.2,";#it{#phi}^{j}");
	HStore()->createTH1F("leadCjet_m",400, 0, 400,";#it{m}_{j} [GeV]");

	HStore()->createTH1F("jgamgam_m",200, 0, 400,";#it{m}_{j#gamma#gamma} [GeV]");
	HStore()->createTH1F("jgamgam_pt",200, 0, 400,";#it{p}^{j#gamma#gamma}_{T} [GeV]");
	HStore()->createTH1F("jgamgam_eta",25, -2.5, 2.5,";#it{#eta}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_y",25, -2.5, 2.5,";#it{y}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_phi",30,-3.2, 3.2,";#it{#phi}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_dR",100, 0., 10.,";#it{#phi}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_dphi",64, -8., 8.,";#it{#phi}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_dy",50, -5., 5.,";#it{#phi}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_dR_b",100, 0., 10.,";#it{#phi}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_dphi_b",64, -8., 8.,";#it{#phi}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_dy_b",50, -5., 5.,";#it{#phi}^{j#gamma#gamma}");
	HStore()->createTH1F("jgamgam_m_b",400, 0, 400,";#it{#phi}^{j#gamma#gamma}");


	//after passing no b tagged jets
	HStore()->createTH1F("m_yy_j25_mv260_jvt", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
	//after passing the c tagging - noefficiency applied
	HStore()->createTH1F("m_yy_j25_mv260_ctag", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");

	HStore()->createTH1F("jet_pT", 200, 0,200,";#it{p}_{T}^{jet} [GeV]");


    //the same but with applying the efficiency as event weight
	//other - weights for the events
	HStore()->createTH1F("total_weights_calc", 100,-1,2,";weights_calc_tot");
    HStore()->createTH1F("total_weights_Ntup",100,-1,2,";weights_ntup_tot");
    HStore()->createTH1F("jvt_plus_total",100,-1,2,";weights_plus_jvt");
    HStore()->createTH1F("jvt_weight",1000,-1,2,";jvt_weight");


    HStore()->createTH1F("truth_tagged_C",2,-0.5,1.5,";Lead_C");
    HStore()->createTH1F("truth_tagged_B",2,-0.5,1.5,";Lead_B");
    HStore()->createTH1F("truth_tagged_L",2,-0.5,1.5,";Lead_L");

    HStore()->createTH1F("truth_jets_AfterCtag_c",4,0,4,";Truth Label");
    HStore()->createTH1F("truth_jets_AfterCtag_b",4,0,4,";Truth Label");
    HStore()->createTH1F("truth_jets_AfterCtag_l",4,0,4,";Truth Label");


	//From the hist store
	for (auto *histo : m_hStore->getListOfHistograms())
		{wk()->addOutput(histo);}





    if(_Dooptimization){

		for(int k = 0; k<bins; k++){
			for(int j =0; j<fbins; j++){
				masses_mv60[k][j] = new TH1D(Form("masses_mv60_%d_%d",k,j),"masses_mv60",60, 100, 160);
				masses_mv70[k][j] = new TH1D(Form("masses_mv70_%d_%d",k,j),"masses_mv70",60, 100, 160);
				masses_mv77[k][j] = new TH1D(Form("masses_mv77_%d_%d",k,j),"masses_mv77",60, 100, 160);

				//for the efficiency
				masses_mv60_nodl1[k][j] = new TH1D(Form("masses_mv60_nodl1_%d_%d",k,j),"masses_mv60_nodl1",60, 100, 160);


				//jvt weight
				masses_mv60_jvt[k][j] = new TH1D(Form("masses_mv60_jvt_%d_%d",k,j),"masses_mvjvt60",60, 100, 160);
				masses_mv70_jvt[k][j] = new TH1D(Form("masses_mv70_jvt_%d_%d",k,j),"masses_mvjvt70",60, 100, 160);
				masses_mv77_jvt[k][j] = new TH1D(Form("masses_mv77_jvt_%d_%d",k,j),"masses_mvjvt77",60, 100, 160);
				//weighted with fractions
				masses_mv60w[k][j] = new TH1D(Form("masses_mv60w_%d_%d",k,j),"masses_mv60w",60, 100, 160);
				masses_mv70w[k][j] = new TH1D(Form("masses_mv70w_%d_%d",k,j),"masses_mv70w",60, 100, 160);
				masses_mv77w[k][j] = new TH1D(Form("masses_mv77w_%d_%d",k,j),"masses_mv77w",60, 100, 160);

 				//efficiencies and numbers of jets
				N_recob[k][j] = new TH1D(Form("N_recob_%d_%d",k,j),"N_recob",20, -0.5, 19.5);
				N_recoc[k][j] = new TH1D(Form("N_recoc_%d_%d",k,j),"N_recoc",20, -0.5, 19.5);
				N_recol[k][j] = new TH1D(Form("N_recol_%d_%d",k,j),"N_recol",20, -0.5, 19.5);
				/////
				N_tagb[k][j]  = new TH1D(Form("N_tagb_%d_%d",k,j),"N_tagb",20, -0.5, 19.5);
				N_tagc[k][j]  = new TH1D(Form("N_tagc_%d_%d",k,j),"N_tagc",20, -0.5, 19.5);
				N_tagl[k][j]  = new TH1D(Form("N_tagl_%d_%d",k,j),"N_tagl",20, -0.5, 19.5);
				//////
				eff_bmiss[k][j]  = new TH1D(Form("eff_bmiss_%d_%d",k,j),"eff_bmiss",20, -0.05, 1.05);
				eff_lmiss[k][j]	 = new TH1D(Form("eff_lmiss_%d_%d",k,j),"eff_lmiss",20, -0.05, 1.05);
				eff_cmiss[k][j]  = new TH1D(Form("eff_cmiss_%d_%d",k,j),"eff_cmiss",20, -0.05, 1.05);
				//
				eff_bmissw[k][j]  = new TH1D(Form("eff_bmissw_%d_%d",k,j),"eff_bmissw",20, -0.05, 1.05);
				eff_lmissw[k][j]  = new TH1D(Form("eff_lmissw_%d_%d",k,j),"eff_lmissw",20, -0.05, 1.05);
				eff_cmissw[k][j]  = new TH1D(Form("eff_cmissw_%d_%d",k,j),"eff_cmissw",20, -0.05, 1.05);
				//
				eff_bmiss_nosf[k][j]  = new TH1D(Form("eff_bmiss_nosf_%d_%d",k,j),"eff_bmiss_nosf",20, -0.05, 1.05);
				eff_lmiss_nosf[k][j]  = new TH1D(Form("eff_lmiss_nosf_%d_%d",k,j),"eff_lmiss_nosf",20, -0.05, 1.05);
				eff_cmiss_nosf[k][j]  = new TH1D(Form("eff_cmiss_nosf_%d_%d",k,j),"eff_cmiss_nosf",20, -0.05, 1.05);
				//
				eff_bmiss_init[k][j]  = new TH1D(Form("eff_bmiss_init_%d_%d",k,j),"eff_bmiss_init",20, -0.05, 1.05);
				eff_lmiss_init[k][j]  = new TH1D(Form("eff_lmiss_init_%d_%d",k,j),"eff_lmiss_init",20, -0.05, 1.05);
				eff_cmiss_init[k][j]  = new TH1D(Form("eff_cmiss_init_%d_%d",k,j),"eff_cmiss_init",20, -0.05, 1.05);



				//////-----------------------------------------
				wk()->addOutput(masses_mv60[k][j]);
				wk()->addOutput(masses_mv70[k][j]);
				wk()->addOutput(masses_mv77[k][j]);

				wk()->addOutput(masses_mv60_nodl1[k][j]);
				//jvt
				wk()->addOutput(masses_mv60_jvt[k][j]);
				wk()->addOutput(masses_mv70_jvt[k][j]);
				wk()->addOutput(masses_mv77_jvt[k][j]);
				//weights frac
				wk()->addOutput(masses_mv60w[k][j]);
				wk()->addOutput(masses_mv70w[k][j]);
				wk()->addOutput(masses_mv77w[k][j]);
				//fracs
				wk()->addOutput(N_recob[k][j]);
				wk()->addOutput(N_recoc[k][j]);
				wk()->addOutput(N_recol[k][j]);
				///
				wk()->addOutput(N_tagb[k][j]);
				wk()->addOutput(N_tagc[k][j]);
				wk()->addOutput(N_tagl[k][j]);
				///
				wk()->addOutput(eff_bmiss[k][j]);
				wk()->addOutput(eff_cmiss[k][j]);
				wk()->addOutput(eff_lmiss[k][j]);
				///
				wk()->addOutput(eff_bmissw[k][j]);
				wk()->addOutput(eff_cmissw[k][j]);
				wk()->addOutput(eff_lmissw[k][j]);
				//
				wk()->addOutput(eff_bmiss_init[k][j]);
				wk()->addOutput(eff_lmiss_init[k][j]);
				wk()->addOutput(eff_cmiss_init[k][j]);
				//
				wk()->addOutput(eff_bmiss_nosf[k][j]);
				wk()->addOutput(eff_lmiss_nosf[k][j]);
				wk()->addOutput(eff_cmiss_nosf[k][j]);




				//if(k==0 and (j%4==0)){//for 41 bin
				if(k==0 and (j%10==0)){
				   	dl1_60[j] = new TH1D(Form("dl1_60_%d",j),"dl1_60",101,-5.5,5.5);
					dl1_70[j] = new TH1D(Form("dl1_70_%d",j),"dl1_70",101,-5.5,5.5);
					dl1_77[j] = new TH1D(Form("dl1_77_%d",j),"dl1_77",101,-5.5,5.5);

					//fracs
					dl1_60w[j] = new TH1D(Form("dl1_60w_%d",j),"dl1_60w",101,-5.5,5.5);
					dl1_70w[j] = new TH1D(Form("dl1_70w_%d",j),"dl1_70w",101,-5.5,5.5);
					dl1_77w[j] = new TH1D(Form("dl1_77w_%d",j),"dl1_77w",101,-5.5,5.5);

					wk()->addOutput(dl1_60[j]);
					wk()->addOutput(dl1_70[j]);
					wk()->addOutput(dl1_77[j]);

					//fracs
					wk()->addOutput(dl1_60w[j]);
					wk()->addOutput(dl1_70w[j]);
					wk()->addOutput(dl1_77w[j]);

				}

			}

		}

/*
		//efficiencies 2d
		j_ctrue = new TH2D("j_ctrue","j_ctrue",100,-5.,5.,100,0.,1.);
		j_btrue = new TH2D("j_btrue","j_btrue",100,-5.,5.,100,0.,1.);
		j_ltrue = new TH2D("j_ltrue","j_ltrue",100,-5.,5.,100,0.,1.);

		c_ctrue = new TH2D("c_ctrue","c_ctrue",100,-5.,5.,100,0.,1.);
		c_btrue = new TH2D("c_btrue","c_btrue",100,-5.,5.,100,0.,1.);
		c_ltrue = new TH2D("c_ltrue","c_ltrue",100,-5.,5.,100,0.,1.);

		wk()->addOutput(j_ctrue);
		wk()->addOutput(j_btrue);
		wk()->addOutput(j_ltrue);

		wk()->addOutput(c_ctrue);
		wk()->addOutput(c_btrue);
		wk()->addOutput(c_ltrue);
*/
 	}//do the optimization



	return EL::StatusCode::SUCCESS;
}

//checking reco plots
EL::StatusCode HcAnalyse :: doreco(const xAOD::PhotonContainer *photons,const xAOD::JetContainer *jets)
{
	auto gamlead = (*photons)[0];
	auto gamsublead = (*photons)[1];
	//1 photon //2 photon
	float y1_pt  = gamlead->pt();   float y2_pt  = gamsublead->pt();
	float y1_eta = gamlead->eta();  float y2_eta = gamsublead->eta();
	float y1_phi = gamlead->phi();  float y2_phi = gamsublead->phi();

	//Higgs candidates
	TLorentzVector ph1 = gamlead->p4(), ph2 = gamsublead->p4();
	ph1 *= HC::invGeV; ph2 *= HC::invGeV; TLorentzVector hcand = ph1+ph2;


	auto leadjet = (*jets)[0];
	TLorentzVector jet = leadjet->p4();
	jet *= HC::invGeV;

	TLorentzVector jgamgam1 = jet + hcand;

	//higgs candates
	HStore()->fillTH1F("myy",hcand.M(),weight_analyse);
	HStore()->fillTH1F("ptyy",hcand.Pt(),weight_analyse);
	HStore()->fillTH1F("yyy",hcand.Rapidity(),weight_analyse);
	HStore()->fillTH1F("dryy",ph1.DeltaR(ph2),weight_analyse);
	HStore()->fillTH1F("dyyy",ph1.Rapidity() - ph2.Rapidity(),weight_analyse);
	HStore()->fillTH1F("dphiyy",ph1.DeltaPhi(ph2),weight_analyse);
	//photons
	HStore()->fillTH1F("y1pt",y1_pt * HC::invGeV,weight_analyse);
	HStore()->fillTH1F("y2pt",y2_pt * HC::invGeV,weight_analyse);
	HStore()->fillTH1F("y1eta",y1_eta,weight_analyse);
	HStore()->fillTH1F("y2eta",y2_eta,weight_analyse);
	HStore()->fillTH1F("y1phi",y1_phi,weight_analyse);
	HStore()->fillTH1F("y2phi",y2_phi,weight_analyse);
	//Drs
	HStore()->fillTH1F("dRjgam1",jet.DeltaR(ph1),weight_analyse);
	HStore()->fillTH1F("dRjgam2",jet.DeltaR(ph2),weight_analyse);
	HStore()->fillTH1F("dphijgam1",jet.DeltaPhi(ph1),weight_analyse);
	HStore()->fillTH1F("dphijgam2",jet.DeltaPhi(ph2),weight_analyse);
	//jet kinematics
	HStore()->fillTH1F("jpt",jet.Pt(),weight_analyse);
	HStore()->fillTH1F("jy",jet.Rapidity(),weight_analyse);
	HStore()->fillTH1F("jeta",jet.PseudoRapidity(),weight_analyse);
	//j+gam+gam
	HStore()->fillTH1F("jgamgam1_pt",jgamgam1.Pt(),weight_analyse);
	HStore()->fillTH1F("jgamgam1_m",jgamgam1.M(),weight_analyse);
	HStore()->fillTH1F("jgamgam1_phi",jgamgam1.Phi(),weight_analyse);
	HStore()->fillTH1F("jgamgam1_eta",jgamgam1.PseudoRapidity(),weight_analyse);
	HStore()->fillTH1F("jgamgam1_y",jgamgam1.Rapidity(),weight_analyse);
	HStore()->fillTH1F("jh_dR",jet.DeltaR(hcand),weight_analyse);
	HStore()->fillTH1F("jh_dphi",jet.DeltaPhi(hcand),weight_analyse);
	HStore()->fillTH1F("jh_dy",jet.Rapidity()-hcand.Rapidity(),weight_analyse);



	return EL::StatusCode::SUCCESS;

}

//--------------------------------------------------------------------------------------------------
//checking truth plots
EL::StatusCode HcAnalyse :: dotruth(const xAOD::TruthParticleContainer *truphotons,const xAOD::EventInfo* HgamTrutheventInfo)
{

	//checking the number of photons
	if (truphotons->size() > 1){
		auto lead = (*truphotons)[0];
		auto sublead = (*truphotons)[1];
		//1 photon //2 photon
		float ph1_pt  = lead->pt();		float ph2_pt  = sublead->pt();
		float ph1_eta = lead->eta();	float ph2_eta = sublead->eta();
		float ph1_phi = lead->phi();	float ph2_phi = sublead->phi();

		//Higgs candidates
		TLorentzVector p1 = lead->p4(), p2 = sublead->p4();
		p1 *= HC::invGeV; p2 *= HC::invGeV; TLorentzVector h = p1+p2;

		//higgs truth
		HStore()->fillTH1F("htrue_m",h.M());
		HStore()->fillTH1F("htrue_pt",h.Pt());
		HStore()->fillTH1F("htrue_y",h.Rapidity());
		HStore()->fillTH1F("htrue_dr",p1.DeltaR(p2));
		HStore()->fillTH1F("htrue_dy",p1.Rapidity() - p2.Rapidity());
		HStore()->fillTH1F("htrue_phi",p1.DeltaPhi(p2));
		//photons truth
		HStore()->fillTH1F("p1true_pt",ph1_pt * HC::invGeV);
		HStore()->fillTH1F("p2true_pt",ph2_pt * HC::invGeV);
		HStore()->fillTH1F("p1true_eta",ph1_eta);
		HStore()->fillTH1F("p2true_eta",ph2_eta);
		HStore()->fillTH1F("p1true_phi",ph1_phi);
		HStore()->fillTH1F("p2true_phi",ph2_phi);

		if(HgamTrutheventInfo->auxdataConst<char>("isFiducial")>0){
			//1 photons
			float ph1pt  = lead->pt();	float ph2pt  = sublead->pt();
			float ph1eta = lead->eta();	float ph2eta = sublead->eta();
			float ph1phi = lead->phi();	float ph2phi = sublead->phi();

			//Higgs candidates
			TLorentzVector p1fid = lead->p4(), p2fid = sublead->p4();
			p1fid *= HC::invGeV; p2fid *= HC::invGeV; TLorentzVector hfid = p1fid+p2fid;

			//fiducial truth
			HStore()->fillTH1F("htruefid_m",hfid.M());
			HStore()->fillTH1F("htruefid_pt",hfid.Pt());
			HStore()->fillTH1F("htruefid_y",hfid.Rapidity());
			HStore()->fillTH1F("htruefid_dr",p1fid.DeltaR(p2fid));
			HStore()->fillTH1F("htruefid_dy",p1fid.Rapidity() - p2fid.Rapidity());
			HStore()->fillTH1F("htruefid_phi",p1fid.DeltaPhi(p2fid));
			HStore()->fillTH1F("p1truefid_pt",ph1pt * HC::invGeV);
			HStore()->fillTH1F("p2truefid_pt",ph2pt * HC::invGeV);
			HStore()->fillTH1F("p1truefid_eta",ph1eta);
			HStore()->fillTH1F("p2truefid_eta",ph2eta);
			HStore()->fillTH1F("p1truefid_phi",ph1phi);
			HStore()->fillTH1F("p2truefid_phi",ph2phi);

			if(HgamTrutheventInfo->auxdataConst<int>("N_j_ctag25")==1){
			  	//1 photons
				float cph1pt  = lead->pt();		float cph2pt  = sublead->pt();
				float cph1eta = lead->eta();	float cph2eta = sublead->eta();
				float cph1phi = lead->phi();	float cph2phi = sublead->phi();

				//Higgs candidates
				TLorentzVector cp1fid = lead->p4(), cp2fid = sublead->p4();
				cp1fid *= HC::invGeV; cp2fid *= HC::invGeV; TLorentzVector chfid = cp1fid+cp2fid;

				//fiducial truth
				HStore()->fillTH1F("chtruefid_m",chfid.M());
				HStore()->fillTH1F("chtruefid_pt",chfid.Pt());
				HStore()->fillTH1F("chtruefid_y",chfid.Rapidity());
				HStore()->fillTH1F("chtruefid_dr",cp1fid.DeltaR(cp2fid));
				HStore()->fillTH1F("chtruefid_dy",cp1fid.Rapidity() - cp2fid.Rapidity());
				HStore()->fillTH1F("chtruefid_phi",cp1fid.DeltaPhi(cp2fid));
				HStore()->fillTH1F("cp1truefid_pt",cph1pt * HC::invGeV);
				HStore()->fillTH1F("cp2truefid_pt",cph2pt * HC::invGeV);
				HStore()->fillTH1F("cp1truefid_eta",cph1eta);
				HStore()->fillTH1F("cp2truefid_eta",cph2eta);
				HStore()->fillTH1F("cp1truefid_phi",cph1phi);
				HStore()->fillTH1F("cp2truefid_phi",cph2phi);

			}//end of c tagged jet

		}//end of fiducial statement

	}//end of "if" statement

return EL::StatusCode::SUCCESS;
}

//double HcAnalyse::DeltaR(double phi1, double eta1, double phi2, double eta2)
//{
//  double deta=eta1-eta2;
//  double dphi=phi1-phi2;
//  while (dphi < -TMath::Pi() ) dphi += 2*TMath::Pi();
//  while (dphi >  TMath::Pi() ) dphi -= 2*TMath::Pi();
//  return std::sqrt(deta*deta+dphi*dphi);
//}


//if you want to use not the leading jet
//unsigned int thisjet=0;
//				for(;thisjet<jets->size();++thisjet)
//				{
//					if( ((*jets)[thisjet]->pt()*GeV > 25)  && ( fabs( (*jets)[thisjet]->eta() )<2.5 ) ) break;
//				}
//				auto leadjet = (*jets)[thisjet];
//



//Here lies the code for the truth tagging which is now irrelevant - RIP
/*
	//define the variables -----------------------------------------//
		int countcjets_b60 = 0;
		float epsilon_b = 0.0, epsilon_c = 0.0, epsilon_l = 0.0;

        if(isMC){
			// ------ doing the c tagging for the jets using the selected points dl1=0.39 and frac 0.22 ---- ////
			std::cout << "-------------------------------------------------------" << std::endl;
			std::cout << " Doing the c tagging using dl = 0.4 and frac = 0.23    " << std::endl;
			std::cout << "-------------------------------------------------------" << std::endl;

       		//define the variables -----------------------------------------//
			int countTrueBmatch=0, countTrueCmatch=0, countTrueLmatch=0;
			int bmistag =0, ctag=0, lmistag =0;
			//--------------------------------------------------------------//

			//-----------------First lets count number of true c,b, and light jets and count the Number of misidentified-----------------//
    		for(auto recoj: *jets){
    			if(fabs(recoj->eta()) > 2.5)continue;//this is after version mistag_v3,v4
    			if(recoj->pt() < 25 * HC::GeV)continue;//this is after version mistag_v3,v4
    			bool isCjet = false, isBjet = false, isLjet = false;
    			//Checking the truth labeling
    			if(recoj->auxdataConst<int>("HadronConeExclTruthLabelID") == 5){isBjet = true;}
    			if(recoj->auxdataConst<int>("HadronConeExclTruthLabelID") == 4){isCjet = true;}
    			if(recoj->auxdataConst<int>("HadronConeExclTruthLabelID") == 0){isLjet = true;}
    			//setting the booleans
		    	bool truth_b = false; bool truth_l = false; bool truth_c = false;

				if(isBjet){truth_b = true; countTrueBmatch++;}
				if(isCjet){truth_c = true; countTrueCmatch++;}
				if(isLjet){truth_l = true; countTrueLmatch++;}

		    	//DL1
				double pu_j = recoj->auxdataConst<double>("DL1_pu");
				double pc_j = recoj->auxdataConst<double>("DL1_pc");
				double pb_j = recoj->auxdataConst<double>("DL1_pb");
				//Dl
				double dl1discriminant = discriminant(pc_j,pb_j,pu_j,0.23);
				if(truth_b && (dl1discriminant > 0.4)){bmistag++;}
				if(truth_c && (dl1discriminant > 0.4)){ctag++; }
				if(truth_l && (dl1discriminant > 0.4)){lmistag++; }

			}//end of reco jet loop


			int nj = jets->size();
			HStore()->fillTH1F("Nj_total",nj);
			HStore()->fillTH1F("N_trub",countTrueBmatch);
			HStore()->fillTH1F("N_truc",countTrueCmatch);
			HStore()->fillTH1F("N_trul",countTrueLmatch);

			HStore()->fillTH1F("N_trub_missc",bmistag);
			HStore()->fillTH1F("N_truc_missc",ctag);
			HStore()->fillTH1F("N_trul_missc",lmistag);


			//-----------------Second lets make the efficiency for the misidentification-----------------//
        	//misidentification B
			if(countTrueBmatch==0) {epsilon_b = 0.0;}
			else { epsilon_b = (bmistag*1.0)/(1.0*countTrueBmatch);}

			//identification C
			if(countTrueCmatch ==0) {epsilon_c = 0.0;}
        	else { epsilon_c = (ctag*1.0)/(1.0*countTrueCmatch); }

	    	//misidentification L
			if(countTrueLmatch==0) {epsilon_l = 0.0;}
			else { epsilon_l = (lmistag*1.0)/(1.0*countTrueLmatch); }

			//filling the efficiency histograms
			if(countTrueBmatch > 0){
			HStore()->fillTH1F("eff_bmiss",epsilon_b);                      HStore()->fillTH1F("eff_bmissw",epsilon_b,weightjvt);
			HStore()->fillTH1F("eff_bmiss_nosf",epsilon_b,tot_weight_nosf); HStore()->fillTH1F("eff_bmiss_init",epsilon_b,initial_weight);}

			if(countTrueCmatch > 0){
			HStore()->fillTH1F("eff_c",epsilon_c);      					HStore()->fillTH1F("eff_cw",epsilon_c,weightjvt);
			HStore()->fillTH1F("eff_c_nosf",epsilon_c,tot_weight_nosf);  	HStore()->fillTH1F("eff_c_init",epsilon_c,initial_weight);}


			if(countTrueLmatch > 0){
			HStore()->fillTH1F("eff_lmiss",epsilon_l);   					HStore()->fillTH1F("eff_lmissw",epsilon_l,weightjvt);
			HStore()->fillTH1F("eff_lmiss_nosf",epsilon_l,tot_weight_nosf); HStore()->fillTH1F("eff_lmiss_init",epsilon_l,initial_weight);}
        }//end of isMC


           // bool isCjet_tagged = false, isBjet_tagged = false, isLjet_tagged = false;
		    //if(leadjet->auxdataConst<int>("HadronConeExclTruthLabelID") == 4) {isCjet_tagged = true;}
		   // if(leadjet->auxdataConst<int>("HadronConeExclTruthLabelID") == 5) {isBjet_tagged = true;}
		   // if(leadjet->auxdataConst<int>("HadronConeExclTruthLabelID") == 0) {isLjet_tagged = true;}
		   // HStore()->fillTH1F("truth_jets_AfterCtag_c",isCjet_tagged);
		   // HStore()->fillTH1F("truth_jets_AfterCtag_b",isBjet_tagged);
		   // HStore()->fillTH1F("truth_jets_AfterCtag_l",isLjet_tagged);



		//  bool isCjet = false, isBjet = false, isLjet = false;;
		//  if(leadjet->auxdataConst<int>("HadronConeExclTruthLabelID") == 4){ isCjet = true;} HStore()->fillTH1F("truth_tagged_C",isCjet);
        //  if(leadjet->auxdataConst<int>("HadronConeExclTruthLabelID") == 5){ isBjet = true;} HStore()->fillTH1F("truth_tagged_B",isBjet);
    	//  if(leadjet->auxdataConst<int>("HadronConeExclTruthLabelID") == 0){ isLjet = true;} HStore()->fillTH1F("truth_tagged_L",isLjet);


*/
